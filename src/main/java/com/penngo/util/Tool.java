package com.penngo.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Tool {
	public final static String DATA_PATH = "/data";
	public final static Map<String, String> contentTypeMap = new HashMap<String, String>();
	public static String calTime(long time){
		String str = time + " ms";
		BigDecimal t1 = new BigDecimal(time);
		if(time > 100){
			BigDecimal t2 = new BigDecimal(1000);
			t1 = t1.divide(t2, 2, BigDecimal.ROUND_HALF_UP);
			str = t1.doubleValue() + " s";
			
			if(t1.longValue() > 60){
				t2 = new BigDecimal(60);
				t1 = t1.divide(t2, 2, BigDecimal.ROUND_HALF_UP);
				str = t1.doubleValue() + " min";
			}
		}
		
		return str;
	}
	public static String getExportPath(){
		String pattern = "yyyy/MM/dd";
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(new Date());
	}
	public static void getContentType() {
//	    text/html ： HTML格式
//	    text/plain ：纯文本格式      
//	    text/xml ：  XML格式
//	    image/gif ：gif图片格式    
//	    image/jpeg ：jpg图片格式 
//	    image/png：png图片格式
//	   application/xhtml+xml ：XHTML格式
//	   application/xml     ： XML数据格式
//	   application/atom+xml  ：Atom XML聚合格式    
//	   application/json    ： JSON数据格式
//	   application/pdf       ：pdf格式  
//	   application/msword  ： Word文档格式
//	   application/octet-stream ： 二进制流数据（如常见的文件下载）
//	   application/x-www-form-urlencoded ： <form encType=””>中默认的encType，form表单数据被编码为key/value格式发送到服务器（表单默认的提交数据的格式）
//	   另外一种常见的媒体格式是上传文件之时使用的：
//	   multipart/form-data ： 需要在表单中进行文件上传时，就需要使用该格式
		

	}
	
	public static String[] getSql(){
		StringBuilder result = new StringBuilder();
        try{
        	InputStream is = Tool.class.getClassLoader().getResourceAsStream("db.sql");
            
            
        	InputStreamReader isr = new InputStreamReader(is,
                    "UTF-8");
        	BufferedReader br = new BufferedReader(isr);
        	
//            BufferedReader br = new BufferedReader(new FileReader(file));//构造一个BufferedReader类来读取文件
            String s = null;
            while((s = br.readLine())!=null){//使用readLine方法，一次读一行
                result.append(System.lineSeparator()+s);
            }
            br.close();    
            
        }catch(Exception e){
            e.printStackTrace();
        }
        return result.toString().split(";");
    }
	public static void main(String[] args){
//		System.out.println(calTime(100000));
	}
	
}
