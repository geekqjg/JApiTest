package com.penngo.common;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import com.jfinal.core.JFinal;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.dialect.Sqlite3Dialect;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.penngo.model.Project;
import com.penngo.model.UseCase;
import com.penngo.util.Tool;

public class Main {
	public static void initDb() {
		PropKit.use("config.txt");
//		C3p0Plugin c3p0Plugin = new C3p0Plugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim());
//		c3p0Plugin.setDriverClass("org.sqlite.JDBC");
//		//me.add(c3p0Plugin);
//		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
//		//me.add(arp);
//		Sqlite3Dialect dialect = new Sqlite3Dialect();
//		//dialect.
//		arp.setDialect(dialect);
//		c3p0Plugin.start();
//		arp.start();
//		String[] sql = Tool.getSql();
//		System.out.println("configPlugin=========" + sql);
		//Db.update(sql);
//		arp.addMapping("project", Project.class);
//		arp.addMapping("useCase", UseCase.class);
		try {
			Class.forName("org.sqlite.JDBC");
		    Connection conn =
		    DriverManager.getConnection(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim());
		    Statement stat = conn.createStatement();
		    //stat.executeUpdate("drop table if exists people;");
//		    stat.execute("");//.executeUpdate("create table people (name, occupation);");
		    
		    String[] sqls = Tool.getSql();
			//System.out.println("configPlugin=========" + sql);
			//Connection connection = dataSource.getConnection();
			
			for(String sql:sqls) {
				System.out.println("configPlugin=========" + sql);
				//Boolean b = connection.prepareStatement(sql).execute();
				Boolean b = stat.execute(sql);
				System.out.println("configPlugin=========" + b);
			}
			conn.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		//System.out.println("args=====" + args.length);
		initDb();
		int port = 8000; // 默认端口
		if(args.length == 1){
			port = Integer.valueOf(args[0]);
		}
		String root = new File("").getAbsolutePath();
		
		PathKit pathKit = new PathKit();
		File file = new File("webapp");
		if(file.exists() == true) {
			pathKit.setWebRootPath(root + "/webapp");
//			JFinal.start("src/main/webapp", port, "/", 5); //eclipse
			JFinal.start("webapp", port, "/");  //idea
		}
		else {
			pathKit.setWebRootPath(root + "/src/main/webapp");
//			JFinal.start("src/main/webapp", port, "/", 5); //eclipse
			JFinal.start("src/main/webapp", port, "/");  //idea
		}


	}
}
