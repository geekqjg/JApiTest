package com.penngo.common;

import java.io.File;
import java.io.InputStream;
import java.sql.Connection;

import javax.sql.DataSource;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.dialect.Sqlite3Dialect;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.render.ViewType;
import com.jfinal.template.Engine;
import com.penngo.api.ApiController;
import com.penngo.index.IndexController;
import com.penngo.model.Project;
import com.penngo.model.UseCase;
import com.penngo.util.Tool;

/**
 * API引导式配置
 */
public class ApiConfig extends JFinalConfig {
	//public final static String dataPath = "/data/";
	public void configEngine(Engine me){

	}
	/**
	 * 配置常量
	 */
	public void configConstant(Constants me) {
	
		PropKit.use("config.txt");
		me.setDevMode(PropKit.getBoolean("devMode", false));
		me.setViewType(ViewType.FREE_MARKER);
	}
	
	/**
	 * 配置路由
	 */
	public void configRoute(Routes me) {
		me.setBaseViewPath("/");
		me.add("/", IndexController.class, "/index");	// 第三个参数为该Controller的视图存放路径
		me.add("/api", ApiController.class);	
	}
	
	/**
	 * 配置插件
	 */
	public void configPlugin(Plugins me) {
		C3p0Plugin c3p0Plugin = new C3p0Plugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim());
		c3p0Plugin.setDriverClass("org.sqlite.JDBC");
		c3p0Plugin.start();
		//DataSource dataSource = c3p0Plugin.getDataSource();
		//checkTable(dataSource);
		me.add(c3p0Plugin);
		
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		me.add(arp);
		arp.setDialect(new Sqlite3Dialect());
//		c3p0Plugin.start();
//		arp.start();
		
//		Db.update(sql);
		arp.addMapping("project", Project.class);
		arp.addMapping("useCase", UseCase.class);
		//arp.start();
		
		
	}
	public void afterJFinalStart(){
		
	}
	/**
	 * 配置全局拦截器
	 */
	public void configInterceptor(Interceptors me) {
		
	}
	
	/**
	 * 配置处理器
	 */
	public void configHandler(Handlers me) {
		
	}
	
	private void checkTable(DataSource dataSource) {
		try {
			
			String[] sqls = Tool.getSql();
			//System.out.println("configPlugin=========" + sql);
			Connection connection = dataSource.getConnection();
			
			for(String sql:sqls) {
				System.out.println("configPlugin=========" + sql);
				Boolean b = connection.prepareStatement(sql).execute();
				System.out.println("configPlugin=========" + b);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}
